package com.ardinal.kotlinshoppingapp.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Auth(
	@field:SerializedName("token")
	@field:Expose
	var token: String?
)