package com.ardinal.kotlinshoppingapp.base

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import com.ardinal.kotlinshoppingapp.util.CalendarUtil
import com.orhanobut.hawk.Hawk

open class BaseView : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Hawk.init(applicationContext).build()
    }

    fun showToast(context: Context, message: Any) {
        Toast.makeText(context, message.toString(), Toast.LENGTH_SHORT).show()
    }

    fun showDatePicker(context: Context, field: EditText) {
        CalendarUtil(context, field)
    }
}