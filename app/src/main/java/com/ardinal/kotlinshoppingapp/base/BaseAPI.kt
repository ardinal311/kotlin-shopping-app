package com.ardinal.kotlinshoppingapp.base

import com.ardinal.kotlinshoppingapp.BuildConfig
import com.ardinal.kotlinshoppingapp.util.Preferences
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit

class BaseAPI {

    companion object {
        var instance = BaseAPI()
    }

    fun createRequest(withTokenAuth: Boolean = false): Retrofit? {
        return getRetrofitBuilder(withTokenAuth = withTokenAuth)?.build()
    }

    private fun getBaseUrl(): String {
        return BuildConfig.BASE_URL
    }

    private fun getRetrofitBuilder(withTokenAuth: Boolean = false): Retrofit.Builder? {
        try {
            val retrofitBuilder = Retrofit.Builder()
            val httpClient = OkHttpClient.Builder()

            if (withTokenAuth) {
                httpClient.addInterceptor {chain ->
                    val request = chain.request().newBuilder()
                        .addHeader("Authorization", Preferences.instance.userInfo)
                        .build()
                    chain.proceed(request)
                }
            }

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(interceptor)

            httpClient.connectTimeout(50, TimeUnit.SECONDS)
                .writeTimeout(50, TimeUnit.SECONDS)
                .readTimeout(50, TimeUnit.SECONDS)

            retrofitBuilder.apply{
                client(httpClient.build())
                baseUrl(getBaseUrl())
                addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                addConverterFactory(GsonConverterFactory.create())
            }

            return retrofitBuilder
        }
        catch (exception: Throwable) {
            return null
        }
        catch (exception: SocketTimeoutException) {
            return null
        }
    }
}