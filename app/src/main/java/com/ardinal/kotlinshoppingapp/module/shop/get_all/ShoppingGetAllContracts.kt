package com.ardinal.kotlinshoppingapp.module.shop.get_all

import android.annotation.SuppressLint
import android.content.Context
import com.ardinal.kotlinshoppingapp.model.entity.EntityShop

object ShoppingGetAllContracts {

	interface ShoppingGetAllPresenterToViewInterface {
		val context: Context
		var presenter: ShoppingGetAllPresenter?

		fun setupView()
		fun setupLink()
	}

	interface ShoppingGetAllPresentorToInteractorInterface {
		var presenter: ShoppingGetAllPresenter?

		fun fetchAllShopping()
		fun submitDeleteShopping(id: Int)
	}

	interface ShoppingGetAllInteractorToPresenterInterface {
		fun responseSuccess(data: List<EntityShop>?)
		fun responseDeleteSuccess(data: String?)

		fun responseFailed()
	}

	interface ShoppingGetAllViewToPresenterInterface {
		var view: ShoppingGetAllActivity?
		var interector: ShoppingGetAllInteractor?
		var router: ShoppingGetAllRouter?
	}

	interface ShoppingGetAllPresenterToRouterInterface {
		companion object {
			@SuppressLint("StaticFieldLeak")
			var view: ShoppingGetAllActivity? = null

			fun configure() {}
		}

		fun gotoUpdateShopping(id: Int)
	}
}
