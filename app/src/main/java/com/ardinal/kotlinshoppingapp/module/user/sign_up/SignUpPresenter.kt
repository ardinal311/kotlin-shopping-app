package com.ardinal.kotlinshoppingapp.module.user.sign_up

import com.ardinal.kotlinshoppingapp.model.entity.Auth
import com.ardinal.kotlinshoppingapp.model.response.AuthResponse
import com.ardinal.kotlinshoppingapp.util.Preferences

class SignUpPresenter : SignUpContracts.SignUpViewToPresenterInterface,
    SignUpContracts.SignUpInteractorToPresenterInterface {

    override var view: SignUpActivity? = null
    override var interector: SignUpInteractor? = null
    override var router: SignUpRouter? = null

    override fun responseSuccess(data: AuthResponse?) {
        if (data != null) {
            val auth = Auth(data.token)
            Preferences.instance.setUserInfo(auth)
            router?.gotoShoppingMain()
        }
        else {
            view?.showToast(view?.context!!,"Gagal mendapatkan authorized")
        }
    }

    override fun responseFailed() {
        view?.showToast(view?.context!!,"Gagal mendapatkan response dari server")
    }

}
