package com.ardinal.kotlinshoppingapp.module.user.get_all

import android.annotation.SuppressLint
import android.util.Log
import com.ardinal.kotlinshoppingapp.api.ShoppingAPI
import com.ardinal.kotlinshoppingapp.base.BaseAPI
import io.reactivex.android.schedulers.AndroidSchedulers

class UserGetAllInteractor : UserGetAllContracts.UserGetAllPresentorToInteractorInterface {

	override var presenter: UserGetAllPresenter? = null

	@SuppressLint("CheckResult")
	override fun fetchAllUser() {
		BaseAPI.instance.createRequest(true)!!.create(ShoppingAPI::class.java)
			.requestAllUser().observeOn(AndroidSchedulers.mainThread()).subscribe( {
				result ->

				if(result.isSuccessful) {
					val response = result.body()
					Log.d("RESULT_CONTENT", response?.data.toString())
					Log.d("RESULT_MESSAGE", response?.message.toString())
					presenter?.responseSuccess(response?.data)
				}
				else {
					presenter?.responseFailed()
				}
			}, {
					e -> e.printStackTrace()
			})
	}

}
