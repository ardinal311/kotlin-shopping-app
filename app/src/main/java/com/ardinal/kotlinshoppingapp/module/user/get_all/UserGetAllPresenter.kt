package com.ardinal.kotlinshoppingapp.module.user.get_all

import com.ardinal.kotlinshoppingapp.model.entity.EntityUser

class UserGetAllPresenter : UserGetAllContracts.UserGetAllViewToPresenterInterface,
	UserGetAllContracts.UserGetAllInteractorToPresenterInterface {

	override var view: UserGetAllActivity? = null
	override var interector: UserGetAllInteractor? = null
	override var router: UserGetAllRouter? = null

	override fun responseSuccess(data: List<EntityUser>?) {
		view?.showResponse(data)
	}

	override fun responseFailed() {
		view?.showToast(view?.context!!,"Gagal mendapatkan response dari server")
	}

}
