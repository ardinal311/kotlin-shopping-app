package com.ardinal.kotlinshoppingapp.module.shop.main

import android.os.Bundle
import android.app.Activity
import android.content.Context
import android.view.View
import com.ardinal.kotlinshoppingapp.R;
import com.ardinal.kotlinshoppingapp.base.BaseView
import kotlinx.android.synthetic.main.activity_shopping_main.*
import kotlinx.android.synthetic.main.component_toolbar.*

class ShoppingMainActivity : BaseView(), ShoppingMainContracts.ShoppingMainPresenterToViewInterface, View.OnClickListener {

	// MARK: Properties
	override var presenter: ShoppingMainPresenter? = null
	override val context: Context = this

	// MARK: Lifecycle
	override fun onCreate(savedInstanceState: Bundle?) {
		ShoppingMainRouter.configure(this)
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_shopping_main)

		setupView()
	}

	override fun setupView() {
		toolbar_back_button.visibility = View.GONE
		toolbar_title.text = getString(R.string.shopping_menu)
		setupLink()
	}

	override fun setupLink() {
		user_get_all_btn.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}

		shop_create_btn.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}

		shop_get_all_btn.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}

		shop_get_by_id_btn.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}
	}

	override fun onClick(view: View?) {
		when(view?.id) {
			user_get_all_btn.id -> {
				presenter?.router?.gotoAllUser()
			}

			shop_create_btn.id -> {
				presenter?.router?.gotoCreateShopping()
			}

			shop_get_all_btn.id -> {
				presenter?.router?.gotoAllShopping()
			}

			shop_get_by_id_btn.id -> {
				presenter?.router?.gotoByIdShopping()
			}
		}
	}
}
