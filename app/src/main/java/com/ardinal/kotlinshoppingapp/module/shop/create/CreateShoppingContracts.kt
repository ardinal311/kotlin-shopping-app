package com.ardinal.kotlinshoppingapp.module.shop.create

import android.annotation.SuppressLint
import android.content.Context
import com.ardinal.kotlinshoppingapp.model.entity.EntityShop
import com.ardinal.kotlinshoppingapp.model.request.ActionShoppingRequest

object CreateShoppingContracts {

	interface CreateShoppingPresenterToViewInterface {
		val context: Context
		var presenter: CreateShoppingPresenter?

		fun setupView()
		fun setupLink()
	}

	interface CreateShoppingPresentorToInteractorInterface {
		var presenter: CreateShoppingPresenter?

		fun submitCreateShopping(request: ActionShoppingRequest)
	}

	interface CreateShoppingInteractorToPresenterInterface {

		fun responseSuccess(data: EntityShop?)
		fun responseFailed()
	}

	interface CreateShoppingViewToPresenterInterface {
		var view: CreateShoppingActivity?
		var interector: CreateShoppingInteractor?
		var router: CreateShoppingRouter?

	}

	interface CreateShoppingPresenterToRouterInterface {
		companion object {
			@SuppressLint("StaticFieldLeak")
			var view: CreateShoppingActivity? = null

			fun configure() {}
		}

		fun gotoShoppingAll()
	}
}
