package com.ardinal.kotlinshoppingapp.module.user.get_all

import android.annotation.SuppressLint
import android.content.Context
import com.ardinal.kotlinshoppingapp.model.entity.EntityUser

object UserGetAllContracts {

	interface UserGetAllPresenterToViewInterface {
		val context: Context
		var presenter: UserGetAllPresenter?

		fun setupView()
		fun setupLink()
	}

	interface UserGetAllPresentorToInteractorInterface {
		var presenter: UserGetAllPresenter?

		fun fetchAllUser()
	}

	interface UserGetAllInteractorToPresenterInterface {
		fun responseSuccess(data: List<EntityUser>?)
		fun responseFailed()
	}

	interface UserGetAllViewToPresenterInterface {
		var view: UserGetAllActivity?
		var interector: UserGetAllInteractor?
		var router: UserGetAllRouter?

	}

	interface UserGetAllPresenterToRouterInterface {
		companion object {
			@SuppressLint("StaticFieldLeak")
			var view: UserGetAllActivity? = null

			fun configure() {}
		}

	}
}
