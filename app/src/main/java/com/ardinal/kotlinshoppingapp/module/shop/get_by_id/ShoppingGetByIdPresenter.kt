package com.ardinal.kotlinshoppingapp.module.shop.get_by_id

import com.ardinal.kotlinshoppingapp.model.entity.EntityShop

class ShoppingGetByIdPresenter : ShoppingGetByIdContracts.ShoppingGetByIdViewToPresenterInterface,
	ShoppingGetByIdContracts.ShoppingGetByIdInteractorToPresenterInterface {

	override var view: ShoppingGetByIdActivity? = null
	override var interector: ShoppingGetByIdInteractor? = null
	override var router: ShoppingGetByIdRouter? = null

	override fun responseSuccess(data: EntityShop?) {
		view?.showResponse(data)
	}

	override fun responseFailed() {
		view?.showToast(view?.context!!,"Gagal mendapatkan response dari server")
	}

}
