package com.ardinal.kotlinshoppingapp.module.shop.update

import android.annotation.SuppressLint
import android.content.Intent
import com.ardinal.kotlinshoppingapp.module.shop.get_all.ShoppingGetAllActivity

class ShoppingUpdateRouter : ShoppingUpdateContracts.ShoppingUpdatePresenterToRouterInterface {

	companion object {
		@SuppressLint("StaticFieldLeak")
		var view: ShoppingUpdateActivity? = null

		fun configure(activity: ShoppingUpdateActivity) {
			val presenter = ShoppingUpdatePresenter()
			val interactor = ShoppingUpdateInteractor()
			val router = ShoppingUpdateRouter()

			activity.presenter = presenter
			presenter.view = activity
			presenter.router = router
			presenter.interector = interactor
			interactor.presenter = presenter
			view = activity
		}
	}

	override fun gotoAllShopping() {
		val intent = Intent(view?.context, ShoppingGetAllActivity::class.java)
		view?.startActivity(intent)
	}
}