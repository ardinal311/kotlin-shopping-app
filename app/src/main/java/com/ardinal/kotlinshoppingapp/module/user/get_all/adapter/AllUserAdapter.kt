package com.ardinal.kotlinshoppingapp.module.user.get_all.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ardinal.kotlinshoppingapp.R
import com.ardinal.kotlinshoppingapp.model.entity.EntityUser
import kotlinx.android.synthetic.main.component_list_user.view.*

class AllUserAdapter(var data: List<EntityUser> = listOf()) : RecyclerView.Adapter<AllUserViewHolder>() {
	override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): AllUserViewHolder {
		return AllUserViewHolder(
			LayoutInflater.from(viewGroup.context)
				.inflate(R.layout.component_list_user, viewGroup, false)
		)
	}

	override fun getItemCount(): Int {
		return data.size
	}

	override fun onBindViewHolder(holder: AllUserViewHolder, position: Int) {
		holder.bindData(data[position])
	}
}

class AllUserViewHolder(view: View) : RecyclerView.ViewHolder(view) {

	fun bindData(data: EntityUser) {
		itemView.apply {
			list_user_name.text = data.name
			list_user_id.text = data.id.toString()
			list_user_username.text = data.username
			list_user_email.text = data.email
			list_user_phone.text = data.phone
			list_user_address.text = data.address
			list_user_city.text = data.city
			list_user_country.text = data.country
			list_user_postcode.text = data.postcode
		}
	}
}